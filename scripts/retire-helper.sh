#! /bin/sh -e

branch=c9s
vfn=tag-ansible/roles/stream9/vars/main.yml

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cd $DIR/..

if [ "x$1" = "x" ] || [ "x$2" = "x" ] || [ "x$3" != "x" ] ; then
  echo "$0: <ticket> <package>"
  exit 1
fi

if [ ! -f ../gitlab-ops/rhel9-package-seed/retire-gitlab-repo ]; then
  echo " ** No file: ../gitlab-ops/rhel9-package-seed/retire-gitlab-repo"
  exit 1
fi

if [ ! -f ./scripts/koji_retire.py ]; then
  echo " ** No file: ./scripts/koji_retire.py"
  exit 1
fi

if [ ! -f $vfn ]; then
  echo " ** No file: $vfn"
  exit 1
fi

_tkt="$1"
_pkg="$2"

../gitlab-ops/rhel9-package-seed/retire-gitlab-repo \
  --repos "$_pkg" --tickets "$_tkt" --branch $branch

./scripts/koji_retire.py -p "$_pkg"

rm -f nvars
egrep -v "^ *- *$_pkg *\$" $vfn > nvars

# If we only remove a single line from the vars file, use it
if [ "$(wc -l < nvars)" = "$(($(wc -l < $vfn) - 1))" ]; then
  mv nvars $vfn
else
  echo " ** Auto vars file update FAIL."
  echo " Old vars file: $vfn"
  echo " New vars file: ./nvars"
fi


touch git-commit
echo " Removing package \"$_pkg\" due to ticket $_tkt." >> git-commit
echo "Adding to git-commit: git commit --all --signoff --file git-commit"
